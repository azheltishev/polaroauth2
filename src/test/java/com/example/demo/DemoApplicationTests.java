package com.example.demo;

import ch.qos.logback.core.net.server.Client;
import com.example.demo.client.PolarClient;
import com.example.demo.client.TokenResponse;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class DemoApplicationTests {

    @Autowired
    PolarClient polarClient;

    @Test
    void contextLoads() {
        TokenResponse tokenResponse = polarClient.exchangeCodeForToken("SplxlOBeZQQYbYS6WxSbIA");

        System.out.println(tokenResponse);
    }

}
