package com.example.demo.client;


import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.Base64;
import java.util.Collections;

@Component
@RequiredArgsConstructor
public class PolarClient {

    private static final String HOST = "https://polarremote.com/v2";

    private static String CLIENT_ID = "YOUR_CLIENT_ID";
    private static String CLIENT_SECRET = "YOUR_CLIENT_SECRET";

    private static String ENCODED_CREDENTIALS = encodeClientIdAndSecret(CLIENT_ID, CLIENT_SECRET);

    private final RestTemplate restTemplate;

    public TokenResponse exchangeCodeForToken(String code) {
        String url = HOST + "/oauth2/token";

        var headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.setBasicAuth(ENCODED_CREDENTIALS);

        var form = new LinkedMultiValueMap<String, String>();
        form.add("grant_type", "authorization_code");
        form.add("code", code);

        var request = new HttpEntity<MultiValueMap<String, String>>(form, headers);

        ResponseEntity<TokenResponse> responseEntity = restTemplate.postForEntity(url, request, TokenResponse.class);
        TokenResponse tokenResponse = responseEntity.getBody();
        if (tokenResponse != null) {
            tokenResponse
                    .setSuccess(responseEntity.getStatusCodeValue() == 200);
        }

        return tokenResponse;
    }

    private static String encodeClientIdAndSecret(String clientId, String clientSecret) {
        String encoded = Base64.getEncoder().encodeToString(
                String.format("%s:%s", clientId, clientSecret).getBytes()
        );

        return encoded;
    }

}
