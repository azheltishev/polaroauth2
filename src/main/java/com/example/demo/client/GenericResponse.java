package com.example.demo.client;


import lombok.Data;

@Data
public class GenericResponse {

    private boolean isSuccess;

    private String error;
}
